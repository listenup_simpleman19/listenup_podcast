import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    DEBUG = False
    TESTING = False
    SECRET_KEY = os.environ.get('SECRET_KEY',
                                '51f52814-0071-11e6-a247-000ec6c2372c')
    JWT_SECRET_KEY = os.environ.get('JWT_SECRET_KEY', SECRET_KEY)
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL', '')
    SQLALCHEMY_POOL_RECYCLE = 60
    CELERY_CONFIG = {}
    LB = os.environ.get('LB', '')
    URL_PREFIX = '/api/podcast'
    TOKEN_EXPIRE_MINUTES = 1440
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif', 'mp3', 'wav'])
    UPLOAD_FOLDER = 'uploads'
    TEMP_FOLDER = os.path.join(basedir, 'temp')
    MARSHALLER_GET_UPLOAD_URL = os.environ.get('MARSHALLER_URL',
                                               'http://listenup_marshaller:5000/api/marshaller/upload/url')
    USER_AGENT = 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0'
    DEFAULT_HEADERS = {'User-Agent': USER_AGENT}
    EXT_TO_MIME_TYPES = {
        'jpg': 'image/jpeg',
        'jpeg': 'image/jpeg',
        'png': 'image/png',
        'gif': 'image/gif',
        'txt': 'text/plain',
        'pdf': 'application/pdf',
        'mp3': 'audio/mpeg',
        'wav': 'audio/x-wav',
    }


class DevelopmentConfig(Config):
    DEBUG = True
    SESSION_COOKIE_SECURE = False
    TEMPLATES_AUTO_RELOAD = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + basedir + os.sep + 'podcast.db'
    UPLOAD_FOLDER = 'uploads'
    TEMP_FOLDER = 'temp'


class ProductionConfig(Config):
    pass


class TestingConfig(ProductionConfig):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///test.db'
    UPLOAD_FOLDER = './tmp'


config = {
    'development': DevelopmentConfig,
    'production': ProductionConfig,
    'testing': TestingConfig
}
