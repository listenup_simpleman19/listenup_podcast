from flask import current_app as app
from typing import List
from datetime import datetime
from flask_sqlalchemy import SQLAlchemy
from listenup_common.utils import guid

db = SQLAlchemy()


class BaseModel(db.Model):
    __abstract__ = True
    id = db.Column(db.Integer, primary_key=True)
    creation_timestamp = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)


class Podcast(BaseModel):
    __tablename_ = 'podcast'
    guid = db.Column(db.String(32), nullable=False)
    from_rss_feed = db.Column(db.Boolean, nullable=False, default=False)
    title = db.Column(db.String(400), nullable=True)
    author = db.Column(db.String(400), nullable=True)
    description = db.Column(db.String(10000), nullable=True)
    link = db.Column(db.String(400), nullable=True)
    language = db.Column(db.String(100), nullable=True)
    pub_date = db.Column(db.DateTime, nullable=True)
    image_guid = db.Column(db.String(32), nullable=True)
    length = db.Column(db.Integer, nullable=False)
    inherit_channel_ownership = db.Column(db.Boolean, nullable=False, default=True)

    owners: List['PodcastOwner'] = db.relationship("PodcastOwner", back_populates="podcast")

    def __init__(self):
        self.guid = guid()

    def to_dict(self, include_files=False):
        item_dict = {
            'id': self.id,
            'creation_timestamp': self.creation_timestamp,
            'from_rss_feed': self.from_rss_feed,
            'guid': self.guid,
            'title': self.title,
            'description': self.description,
            'language': self.language,
            'pub_date': self.pub_date,
            'image_guid': self.image_guid,
            'length': self.length,
            'inherit_channel_ownership': self.inherit_channel_ownership,
            'owners': [owner.to_dict() for owner in self.owners],
        }

        if include_files:
            files: List[PodcastFile] = PodcastFile.query.filter_by(podcast_guid=self.guid).all()
            item_dict['files'] = [file.to_dict() for file in files]
        return item_dict


class PodcastFile(BaseModel):
    __tablename__ = 'podcast_file'
    guid = db.Column(db.String(32), nullable=False)
    podcast_id = db.Column(db.Integer, nullable=False)
    podcast_guid = db.Column(db.String(32), nullable=False)
    file_guid = db.Column(db.String(32), nullable=False)
    mime_type = db.Column(db.String(100), nullable=True)
    quality = db.Column(db.String(100), nullable=True)

    def __init__(self, item: Podcast, file_guid: str = None):
        self.guid = guid()
        self.podcast_id = item.id
        self.podcast_guid = item.guid
        self.file_guid = file_guid

    def to_dict(self):
        return {
            'id': self.id,
            'guid': self.guid,
            'creation_timestamp': self.creation_timestamp,
            'file_guid': self.file_guid,
            'podcast_guid': self.podcast_guid,
            'mime_type': self.mime_type,
            'quality': self.quality
        }


class PodcastOwner(BaseModel):
    __tablename__ = 'podcast_owner'
    guid = db.Column(db.String(32), nullable=False)
    podcast_id = db.Column(db.Integer, db.ForeignKey('podcast.id'), nullable=False)
    podcast: Podcast = db.relationship("Podcast", back_populates="owners")
    user_guid = db.Column(db.String(32), nullable=False)

    def to_dict(self):
        return {
            'podcast_guid': self.podcast.guid,
            'user_guid': self.user_guid,
        }

    def __init__(self, user_guid: str, podcast: Podcast = None):
        self.guid = guid()
        self.user_guid = user_guid
        if podcast:
            self.podcast = podcast
