from listenup_common.utils import guid as create_guid

from podcast.models import PodcastFile, Podcast, db
from podcast import celery
import os
from listenup_common.wrappers.marshaller import get_upload_url
from listenup_common.wrappers.files import upload_file_to_url
from podcast.audio import get_info, convert_file
from podcast.audio.codecs import OutputEnum
from flask import current_app
from listenup_common.logging import get_logger

RETRY_TIMES = 5

config_name = os.environ.get('LISTENUP_APP_CONFIG', 'development')

logger = get_logger(__name__)


@celery.task
def process_file(filename, filepath, podcast_file: PodcastFile):
    logger.info("Processing podcast file: %s, filepath: %s, for podcast: %s", filename, filepath, podcast_file.guid)
    from podcast import create_app
    app = create_app(config_name)
    with app.app_context():
        logger.debug("Stood up app context now processing podcast: %s", podcast_file.guid)
        if not filename or not filepath or not podcast_file:
            logger.error("Error processing file, file or podcast was null")
            return

        upload_url = get_upload_url(True)
        if not upload_url:
            logger.error("Error getting upload url from marshaller")
            return

        podcast = Podcast.query.filter_by(guid=podcast_file.podcast_guid).one_or_none()
        if not podcast:
            logger.error("Could not find podcast for: %s", podcast_file.podcast_guid)
            return

        logger.debug("Processing file: %s", filepath)
        info = get_info(filepath)
        podcast.length = info.audio_streams[0].duration

        logger.debug("Converting file to known type")
        temp_file = os.path.join(app.root_path, app.config['TEMP_FOLDER'], create_guid())
        if not convert_file(filepath, temp_file, OutputEnum.MP3_128kbps):
            logger.error("Failed to convert file: %s", filepath)
            return

        file = upload_file_to_url(filename + '.mp3', temp_file, upload_url)
        if not file or not file.get('guid_file_name'):
            logger.error("Failed to upload file: %s", filepath)
            return

        podcast_file.file_guid = file.get('guid_file_name')
        logger.debug("Got a file guid: %s so upload was a success", podcast_file.file_guid)
        db.session.add(podcast)
        db.session.add(podcast_file)
        db.session.commit()
        logger.debug("Saved podcast file guid: %s to the database with the new file", podcast_file.guid)
        logger.debug("Removing temporary file: %s", temp_file)
        os.remove(temp_file)
        logger.debug("Removing temporary file: %s", filepath)
        os.remove(filepath)

