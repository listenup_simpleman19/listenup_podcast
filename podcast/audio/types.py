from typing import List


class AudioStreamInfo(object):
    def __init__(self,
                 bitrate: int = None,
                 codec: str = None,
                 duration: int = None,
                 sample_rate: int = None,
                 channels: int = None,
                 ):
        self.bitrate = bitrate
        self.codec = codec
        self.duration = duration
        self.sample_rate = sample_rate
        self.channels = channels


class VideoStreamInfo(object):
    def __init__(self,
                 bitrate: int = None,
                 codec: str = None,
                 duration: int = None,
                 width: int = None,
                 height: int = None,
                 framerate: int = None,
                 ):
        self.bitrate = bitrate
        self.codec = codec
        self.duration = duration
        self.width = width
        self.height = height
        self.framerate = framerate


class FileFormat(object):
    def __init__(self,
                 filename: str = None,
                 size: int = None,
                 bitrate: int = None,
                 ):
        self.filename = filename
        self.size = size
        self.bitrate = bitrate


class FileInfo(object):
    def __init__(self,
                 audio_streams: List[AudioStreamInfo] = None,
                 video_streams: List[VideoStreamInfo] = None,
                 file_format: FileFormat = None,
                 failed: bool = False):
        self.audio_streams: List[AudioStreamInfo] = audio_streams
        self.video_streams: List[VideoStreamInfo] = video_streams
        self.file_format: FileFormat = file_format
        self.failed = failed

    @property
    def is_video(self) -> bool:
        return len(self.video_streams) != 0

    @property
    def duration(self) -> int:
        for stream in self.audio_streams:
            if stream.duration != 0:
                return stream.duration
        for stream in self.video_streams:
            if stream.duration != 0:
                return stream.duration
        return 0
