from enum import Enum

VIDEO_CODECS = ['h264']

AUDIO_CODECS = ['aac', 'mp3']


class OutputEnum(Enum):
    MP3_128kbps = ('mp3', '128k')
