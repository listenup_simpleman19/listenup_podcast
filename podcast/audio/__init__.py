# FFMPEG Wrapper for audio
from typing import Optional, List, Tuple
import subprocess
import json
from listenup_common.logging import get_logger
from podcast.audio.codecs import VIDEO_CODECS, AUDIO_CODECS, OutputEnum
from podcast.audio.types import FileInfo, AudioStreamInfo, VideoStreamInfo, FileFormat

logger = get_logger(__name__)


def convert_file(src_filepath: str, dest_filepath: str, output_codec: OutputEnum) -> bool:
    stdout, stderr, return_code = run_and_get_output(
        ['ffmpeg', '-i', src_filepath, '-f', output_codec.value[0], '-b:a', output_codec.value[1], dest_filepath]
    )
    if return_code != 0:
        logger.warn("Failed to run ffmpeg: %s : %s", stdout, stderr)
        return False
    logger.info("Successfully converted file: %s to %s", src_filepath, output_codec)
    return True


def get_info(filepath: str) -> Optional[FileInfo]:
    stdout, stderr, return_code = run_and_get_output(
        ['ffprobe', '-v', 'quiet', '-print_format', 'json', '-show_format', '-show_streams', filepath]
    )
    if return_code == 0:
        parsed_json = json.loads(stdout)
        logger.info("Got info: %s", parsed_json)
        file_format = parsed_json.get('format')
        file_streams = parsed_json.get('streams')
        audio_streams = []
        video_streams = []
        for stream in file_streams:
            if stream.get('codec_name') in VIDEO_CODECS:
                framerate = stream.get('avg_frame_rate')
                if framerate:
                    framerate = framerate.split('/')[0]
                    framerate = int(framerate)
                else:
                    framerate = 0
                video_streams.append(VideoStreamInfo(
                    bitrate=int(stream.get('bit_rate', 0)),
                    codec=stream.get('codec_name'),
                    duration=int(float(stream.get('duration', 0))),
                    width=int(stream.get('coded_width', 0)),
                    height=int(stream.get('coded_height', 0)),
                    framerate=framerate,
                ))
            elif stream.get('codec_name') in AUDIO_CODECS:
                audio_streams.append(AudioStreamInfo(
                    bitrate=int(stream.get('bit_rate', 0)),
                    codec=stream.get('codec_name'),
                    duration=int(float(stream.get('duration', 0))),
                    sample_rate=int(stream.get('sample_rate', 0)),
                    channels=int(stream.get('channels', 0)),
                ))
            else:
                logger.error("Could not find codec: %s in known codecs for file: %s",
                             stream.get('codec_name'), filepath)
        file_format_obj = None
        if file_format:
            file_format_obj = FileFormat(
                filename=file_format.get('filename'),
                size=int(file_format.get('size', 0)),
                bitrate=int(file_format.get('bit_rate', 0)),
            )
        return FileInfo(audio_streams=audio_streams, video_streams=video_streams, file_format=file_format_obj)
    return None


def run_and_get_output(command: List[str]) -> Tuple[Optional[str], Optional[str], int]:
    result = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    return result.stdout.decode('utf-8'), result.stderr.decode('utf-8'), result.returncode
