import os
from typing import List
from flask import Blueprint, render_template, session, request, current_app as app, g
from werkzeug.utils import secure_filename
from listenup_common.api_response import ApiException, ApiMessage, ApiResult
from listenup_common.auth import token_auth
from podcast.models import Podcast, PodcastFile, PodcastOwner
from podcast.tasks import process_file
from listenup_common.utils import guid as create_guid, require_json
from listenup_common.logging import get_logger
from . import db
import datetime

main = Blueprint('main', __name__, url_prefix='/api/podcast')

root_path = os.path.dirname(os.path.abspath(__file__))

logger = get_logger(__name__)


@main.route("/add", methods=["POST"])
@token_auth.login_required
@require_json()
def add_podcast(json):
    logger.info("Creating a new podcast")
    content = json
    podcast = Podcast()
    podcast.title = content.get('title', None)
    podcast.description = content.get('description', None)
    podcast.author = content.get('author', None)
    podcast.language = content.get('language', None)
    podcast.from_rss_feed = content.get('from_rss_feed', False)
    if content.get('pub_date'):
        try:
            podcast.pub_date = datetime.datetime.strptime(content.get('pub_date'), '%Y-%m-%d %H:%M:%S.%f')
        except ValueError:
            logger.error(f"Failed to parse: {content.get('pub_date')} defaulting to now")
            podcast.pub_date = datetime.datetime.now()
    else:
        podcast.pub_date = datetime.datetime.now()
    podcast.length = -1
    owner = PodcastOwner(podcast=podcast, user_guid=g.current_user_guid)
    db.session.add(podcast)
    db.session.add(owner)
    db.session.commit()
    return_dict = podcast.to_dict()
    return_dict['upload_url'] = '/api/podcast/' + podcast.guid + "/upload"
    logger.info("Successfully created podcast guid: %s, returning upload url: %s", podcast.guid, return_dict['upload_url'])
    return ApiResult(value=return_dict, status=200).to_response()


@main.route("/<string:guid>/upload", methods=["PUT"])
@token_auth.login_required
def upload_file(guid):
    logger.info("Processing upload file for podcast: %s", guid)
    podcast = Podcast.query.filter_by(guid=guid).one_or_none()
    if not podcast:
        raise ApiException(message="Could not find podcast for guid: " + guid, status=404)

    if not is_owner(podcast, g.current_user_guid):
        raise ApiException(message="Can not upload to a podcast that you are not the owner of", status=401)

    if 'file' not in request.files:
        raise ApiException(message="No file in request", status=403)

    file = request.files['file']
    filename = request.form.get('filename', None)
    if not filename:
        filename = file.filename
        logger.info("Could not find filename in form so using filename of upload: %s", filename)

    if not filename:
        raise ApiException(message="Could not find filename", status=403)

    if file:
        filename = secure_filename(file.filename)
        logger.debug("File name is: %s", filename)
        location = os.path.join(app.root_path, app.config['TEMP_FOLDER'])
        new_file = PodcastFile(podcast)
        filepath = os.path.join(location, create_guid())
        file.save(filepath)
        logger.debug("Saving podcast to: %s", filepath)
        process_file.apply_async(args=[filename, filepath, new_file])
        return ApiResult(value=new_file.to_dict(), status=200).to_response()
    else:
        raise ApiException(message="Invalid file type", status=403)


@main.route("/<string:guid>")
def get_podcast(guid):
    logger.info("Getting podacst for guid: %s", guid)
    podcast: Podcast = Podcast.query.filter_by(guid=guid).one_or_none()
    if not podcast:
        logger.error("Could not find podcast for guid: %s", guid)
        raise ApiException(message="Could not find podcast for guid: " + guid, status=404)
    return ApiResult(value=podcast.to_dict(include_files=True), status=200).to_response()


@main.route("/<string:guid>/owners", methods=["GET"])
def get_podcast_owners(guid):
    logger.info("Getting podcast owners for guid: %s", guid)
    podcast: Podcast = Podcast.query.filter_by(guid=guid).one_or_none()
    if not podcast:
        raise ApiException(message="Could not find podcast for guid: " + guid, status=404)
    return ApiResult(value=[owner.to_dict() for owner in podcast.owners], status=200)


@main.route("/<string:guid>/owner", methods=["POST"])
@token_auth.login_required
@require_json(required_attrs=['user_guid'])
def add_podcast_owner(guid, json):
    logger.info("Adding podcast owner for guid: %s", guid)
    user_guid = json['user_guid']
    podcast: Podcast = Podcast.query.filter_by(guid=guid).one_or_none()
    if not podcast:
        raise ApiException(message="Could not find podcast for guid: " + guid, status=404)

    if not is_owner(podcast, g.current_user_guid):
        raise ApiException(message="Ownership can not be changed if you are not an owner of the podcast", status=401)

    owner: PodcastOwner = PodcastOwner.query.filter_by(podcast=podcast, user_guid=user_guid)
    if not owner:
        raise ApiException(message="User is already an owner of podcast", status=201)

    new_owner = PodcastOwner(podcast=podcast, user_guid=user_guid)
    db.session.add(new_owner)
    db.session.commit()
    return get_podcast(guid)


@main.route("/<string:guid>/owner", methods=["DELETE"])
@token_auth.login_required
@require_json(required_attrs=['user_guid'])
def delete_podcast_owner(guid, json):
    logger.info("Removing podcast owner for guid: %s", guid)
    user_guid = json['user_guid']
    podcast: Podcast = Podcast.query.filter_by(guid=guid).one_or_none()
    if not podcast:
        raise ApiException(message="Could not find podcast for guid: " + guid, status=404)

    if not is_owner(podcast, g.current_user_guid):
        raise ApiException(message="Ownership can not be changed if you are not an owner of the podcast", status=401)

    if len(podcast.owners) == 1:
        raise ApiException(message="Only one owner left, podcast must have atleast one owner")

    owner: PodcastOwner = PodcastOwner.query.filter_by(podcast=podcast, user_guid=user_guid)
    if not owner:
        raise ApiException(message="User is not an owner of podcast", status=201)

    db.session.delete(owner)
    db.session.commit()
    return get_podcast(guid)


@main.route("/list")
@require_json()
def get_podcast_list(json):
    podcast_guids = json
    logger.info("Getting a list of podcasts for guids: %s", podcast_guids)
    podcasts: List[Podcast] = Podcast.query.filter(Podcast.guid.in_(podcast_guids)).all()
    if len(podcasts) != len(podcast_guids):
        logger.error("Did not find some of the podcasts in: %s", podcast_guids)
    return ApiResult(value=[podcast.to_dict(include_files=True) for podcast in podcasts]).to_response()


@main.route("/all")
def get_all():
    podcasts = Podcast.query.all()
    return ApiResult(value=[podcast.to_dict(include_files=True) for podcast in podcasts], status=200).to_response()


def is_owner(podcast: Podcast, user_guid: str) -> bool:
    for owner in podcast.owners:
        if owner.user_guid == user_guid:
            return True
    return False


@main.errorhandler(ApiException)
def handle_api_exception(error: ApiException):
    return error.to_result().to_response()
