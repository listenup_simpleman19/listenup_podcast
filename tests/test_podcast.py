import unittest
from podcast.models import RssItem, RssFeed
from podcast import create_app, db
from config import TestingConfig
from listenup_common.test import FlackTestCase
import os

rss_feeds = [
    "https://feedpress.me/atxplained",
    "https://feedpress.me/kutinblackamerica",
    "https://rss.art19.com/tim-ferriss-show",
    "https://level1techs.com/podcasts/feed",
    "https://feedpress.me/2GOYH",
    "https://www.npr.org/rss/podcast.php?id=510313",
    "https://www.npr.org/rss/podcast.php?id=381444908",
]


class TestRSS(FlackTestCase):
    def setUp(self):
        self.app = create_app('testing')
        self.ctx = self.app.app_context()
        self.ctx.push()
        db.session.commit()
        db.drop_all()
        db.create_all()
        self.client = self.app.test_client()

    def test_add(self):
        for url in rss_feeds:
            r, s, h = self.post('/api/rss/add', data={
                'feed_url': url
            })
            self.assertEqual(s, 201)
            print(r)
